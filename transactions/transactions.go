package transactions

import (
	"encoding/json"
	"fmt"
	"haproxyAPI/client"
	"haproxyAPI/utils"
	"net/http"
)

type Client = client.Client

func (t *Transaction) GetAllTransactions(client *Client) ([]*Transaction, []byte, error) {
	body, resp, err := utils.SendHTTPRequest("GET", baseURL, client, nil)
	var transactions []*Transaction
	err = utils.HandleResponse(resp, body, http.StatusOK, &transactions)
	if err != nil {
		return nil, []byte(string(body)), fmt.Errorf("unexpected status code: %d", resp.StatusCode)

	}
	return transactions, body, nil

}

func (t *Transaction) GetTransaction(id string, client *Client) (*Transaction, []byte, error) {
	url := fmt.Sprintf(baseURL+"%s", id)
	body, resp, err := utils.SendHTTPRequest("GET", url, client, nil)
	newTransaction := &Transaction{}
	err = utils.HandleResponse(resp, body, http.StatusOK, newTransaction)
	if err != nil {

		return nil, []byte(string(body)), fmt.Errorf("unexpected status code: %d", resp.StatusCode)
	}
	return newTransaction, body, nil
}

func (t *Transaction) CommitTransaction(id string, client *Client) (*Transaction, []byte, error) {
	url := fmt.Sprintf(baseURL+"%s", id)
	body, resp, err := utils.SendHTTPRequest("PUT", url, client, nil)
	if resp.StatusCode == http.StatusOK || resp.StatusCode == http.StatusAccepted {
		newTransaction := &Transaction{}

		err = json.Unmarshal(body, newTransaction)
		if err != nil {
			return nil, body, err
		}

		// Return the matched Transaction struct
		return newTransaction, body, nil
	} else {
		return nil, []byte(string(body)), fmt.Errorf("unexpected status code: %d", resp.StatusCode)
	}

}
func (t *Transaction) Deleteransaction(id string, client *Client) ([]byte, error) {
	url := fmt.Sprintf(baseURL+"%s", id)
	body, resp, err := utils.SendHTTPRequest("DELETE", url, client, nil)
	if resp.StatusCode != http.StatusNoContent || err != nil {
		return []byte(string(body)), fmt.Errorf("unexpected status code: %d", resp.StatusCode)

	}

	return body, nil
}
func (t *Transaction) CreateTransaction(version string, client *Client) (*Transaction, []byte, error) {
	queryParams := map[string]string{
		"version": version,
	}
	u, err := utils.AddQueryParams(baseURL, queryParams)
	body, resp, err := utils.SendHTTPRequest("POST", u, client, nil)
	newTransaction := &Transaction{}
	err = utils.HandleResponse(resp, body, http.StatusCreated, newTransaction)
	if err != nil {
		fmt.Println("test")
		return nil, []byte(string(body)), fmt.Errorf("unexpected status code: %d", resp.StatusCode)
	}

	return newTransaction, body, nil

}
