package transactions

var baseURL = "http://localhost:55/v2/services/haproxy/transactions/"

type TransactionStatus string

const (
	StatusInProgress TransactionStatus = "in_progress"
	StatusCompleted  TransactionStatus = "success"
	StatusFailed     TransactionStatus = "failed"
	StatusOutdated   TransactionStatus = "outdated"
)

type Transaction struct {
	Version int    `json:"_version"`
	ID      string `json:"id"`
	Status  string `json:"status"`
}
