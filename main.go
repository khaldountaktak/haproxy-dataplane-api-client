package main

import (
	// "encoding/json"
	"fmt"
	"haproxyAPI/backend"
	"haproxyAPI/client"
	"haproxyAPI/transactions"
)

type Client = client.Client
type Transaction = transactions.Transaction
type Back = backend.Back
var BuildVersion string

func main() {
	fmt.Println(BuildVersion)
	// Create a new Basic Auth client
	// client := client.NewBasicAuthClient("khaldoun", "khaldoun")
	// fmt.Println("Client Username:", client.Username)
	// fmt.Println("Client Password:", client.Password)

	// transaction := Transaction{}
	// filePath := "/etc/haproxy/haproxy.cfg"
	// version, err := utils.ExtractVersionNumber(filePath)
	// if err != nil {
	// 	fmt.Println("Error:", err)
	// 	return
	// }

	// fmt.Println("Version:", version)
	// transactions, resp, err := transaction.GetAllTransactions(client)
	// if err != nil {
	// 	fmt.Println("Error:", err)
	// 	fmt.Print(string(resp))
	// 	return
	// }

	// for _, t := range transactions {
	// 	fmt.Println("Version:", t.Version)
	// 	fmt.Println("ID:", t.ID)
	// 	fmt.Println("Status:", t.Status)
	// 	fmt.Println("----------------------")
	// }
	// exist, respBody, err := transaction.GetTransaction("dc943ac3-8141-4ec0-aca7-a0e3b2a85db9", client)
	// if err != nil {
	// 	fmt.Println("error")
	// 	fmt.Println(string(respBody))
	// 	return
	// }
	// fmt.Println(exist.Status)
	// exist, respBody, err := transaction.CommitTransaction("ea533556-c541-41d4-b457-b5c735c770ca", client)
	// if err != nil {
	// 	fmt.Println(err)
	// 	fmt.Println(string(respBody))
	// 	return
	// }
	// fmt.Println(exist.ID)

	// newTransaction, respBody, err := transaction.CreateTransaction(version, client)
	// if err != nil {
	// 	fmt.Println("Error:", err)
	// 	fmt.Println(string(respBody))
	// 	return
	// }
	// fmt.Println("ID:", newTransaction.ID)
	// fmt.Println("Status:", newTransaction.Status)
	// respBody, err := transaction.Deleteransaction("3c4629a0-ede7-442d-8797-7d2a29e54907", client)
	// if err != nil {
	// 	fmt.Println("Error:", err)
	// 	fmt.Println(string(respBody))
	// 	return
	// }
	// fmt.Println("success")

	// -------------------------------------------------
	// ------------ BACKEND
	// -------------------------------------------------
	// jsonStr := `{
		// "adv_check": "httpchk",
		// "balance": {
		// "algorithm": "roundrobin"
		// },
		// "forwardfor": {
		// "enabled": "enabled"
		// },
		// "httpchk_params": {
		// "method": "POST",
		// "uri": "/check",
		// "version": "HTTP/1.1"
		// },
		// "mode": "http",
		// "name": "testt_backend"
		// }`

	// Parse the JSON string into a Back struct
	// var back *backend.Back = &backend.Back{}
	// err := json.Unmarshal([]byte(jsonStr), back)
	// if err != nil {
	// 	fmt.Println("Error:", err)
	// 	return
	// }
	// fmt.Println(*back)
	// newTransaction := &Transaction{
	// 	ID:     "471e0faf-0a38-41e9-aaa6-bc66735ef252",
	// 	Status: "in_progress",
	// }
	// backend, body, err := back.GetBackend(client, newTransaction, "testt_backend")
	// if err != nil {
	// 	fmt.Println(err)
	// 	fmt.Println(string(body))
	// }
	// fmt.Println(backend.Data.AdvCheck)
	// backendType := reflect.TypeOf(*back)
	// for i := 0; i < backendType.NumField(); i++ {
	// 	field := backendType.Field(i)
	// 	value := reflect.ValueOf(backend.Data).Field(i).Interface()
	// 	fmt.Printf("%s: %v\n", field.Name, value)
	// }

	// x, body, err := back.GetAllBackend(client, newTransaction)
	// fmt.Println(string(body))
	// fmt.Println(x)
	// x, body, err := back.DeleteBackend(client, newTransaction, "backend")
	// x, body, err := back.UpdateBackend(client, back, newTransaction, "backend")
	// fmt.Println(err)
	// fmt.Println(err)
	// fmt.Print(string(body))
	// fmt.Println(x)
}
