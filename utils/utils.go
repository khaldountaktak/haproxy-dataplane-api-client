package utils

import (
	"bytes"
	"encoding/json"
	"fmt"
	"haproxyAPI/client"
	"io/ioutil"
	"net/http"
	"net/url"
	"regexp"
)

type Client = client.Client

func ExtractVersionNumber(filePath string) (string, error) {
	fileContent, err := ioutil.ReadFile(filePath)
	if err != nil {
		return "", fmt.Errorf("error reading file: %s", err)
	}

	// Define the regular expression pattern
	pattern := `# _version=(\d+)`

	// Compile the regular expression
	regex := regexp.MustCompile(pattern)

	// Find the version number in the file content
	matches := regex.FindStringSubmatch(string(fileContent))

	if len(matches) >= 2 {
		fmt.Println(matches[1])
		version := matches[1]
		return version, nil
	} else {
		return "", fmt.Errorf("version not found in the file")
	}
}

func SendHTTPRequest(method, url string, client *Client, body []byte) ([]byte, *http.Response, error) {
	req, err := http.NewRequest(method, url, bytes.NewBuffer(body))
	if err != nil {
		return nil, nil, err
	}
	req.SetBasicAuth(client.Username, client.Password)
	req.Header.Set("Content-Type", "application/json")

	clientt := http.Client{}
	resp, err := clientt.Do(req)
	if err != nil {
		return nil, nil, err
	}
	defer resp.Body.Close()

	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, nil, err
	}

	return respBody, resp, nil
}

func HandleResponse(apiResp *http.Response, body []byte, expectedStatus int, target interface{}) error {
	if apiResp.StatusCode != expectedStatus {

		return fmt.Errorf("unexpected status code: %d", apiResp.StatusCode)
	}
	// fmt.Println(body)
	// fmt.Println("------------------------")
	// fmt.Println(target)
	err := json.Unmarshal(body, target)
	// fmt.Println(err)

	if err != nil {
		fmt.Println(err)
		return fmt.Errorf("error unmarshaling response body: %w", err)
	}

	return nil
}
func AddQueryParams(baseURL string, queryParams map[string]string) (string, error) {
	u, err := url.Parse(baseURL)
	if err != nil {
		return "", err
	}

	q := u.Query()
	for key, value := range queryParams {
		q.Set(key, value)
	}

	u.RawQuery = q.Encode()

	return u.String(), nil
}
