package backend

import (
	"encoding/json"
	"fmt"
	"haproxyAPI/client"
	"haproxyAPI/transactions"
	"haproxyAPI/utils"
	"net/http"
)

type Client = client.Client
type Transaction = transactions.Transaction

func (b *Back) CreateBackend(client *Client, back *Back, transaction *Transaction) (*Back, []byte, error) {
	jsonBack, err := json.Marshal(back)
	queryParams := map[string]string{
		"transaction_id": transaction.ID,
	}
	// fmt.Println(string(jsonBack))
	u, err := utils.AddQueryParams(baseURL, queryParams)
	body, resp, err := utils.SendHTTPRequest("POST", u, client, jsonBack)
	newBackend := &Back{}
	err = utils.HandleResponse(resp, body, http.StatusAccepted, newBackend)
	if err != nil {
		return nil, []byte(string(body)), fmt.Errorf("unexpected status code: %d", resp.StatusCode)

	}
	return newBackend, body, nil
}

func (b *Back) GetBackend(client *Client, transaction *Transaction, name string) (*Backend, []byte, error) {
	queryParams := map[string]string{
		"transaction_id": transaction.ID,
	}
	// fmt.Println(string(jsonBack))
	u, err := utils.AddQueryParams(baseURL+name, queryParams)
	fmt.Println(u)
	body, resp, err := utils.SendHTTPRequest("GET", u, client, nil)
	newBackend := &Backend{}
	err = utils.HandleResponse(resp, body, http.StatusOK, newBackend)
	if err != nil {
		return nil, []byte(string(body)), fmt.Errorf("unexpected status code: %d", resp.StatusCode)

	}
	return newBackend, body, nil
}

func (b *Back) GetAllBackend(client *Client, transaction *Transaction) ([]Back, []byte, error) {
	queryParams := map[string]string{
		"transaction_id": transaction.ID,
	}
	// fmt.Println(string(jsonBack))
	u, err := utils.AddQueryParams(baseURL, queryParams)
	body, resp, err := utils.SendHTTPRequest("GET", u, client, nil)
	response := &Backends{}
	err = utils.HandleResponse(resp, body, http.StatusOK, response)
	if err != nil {
		return nil, []byte(string(body)), fmt.Errorf("unexpected status code: %d", resp.StatusCode)

	}
	return response.Data, body, nil
}

func (b *Back) DeleteBackend(client *Client, transaction *Transaction, name string) (string, []byte, error) {
	queryParams := map[string]string{
		"transaction_id": transaction.ID,
	}
	// fmt.Println(string(jsonBack))
	u, err := utils.AddQueryParams(baseURL+name, queryParams)
	fmt.Println(u)
	body, resp, err := utils.SendHTTPRequest("DELETE", u, client, nil)
	if err != nil {
		return "", body, fmt.Errorf("unexpected status code: %d", resp.StatusCode)

	}
	return "success", body, nil
}

func (b *Back) UpdateBackend(client *Client, back *Back, transaction *Transaction, name string) (*Backend, []byte, error) {
	jsonBack, err := json.Marshal(back)
	queryParams := map[string]string{
		"transaction_id": transaction.ID,
	}
	// fmt.Println(string(jsonBack))
	u, err := utils.AddQueryParams(baseURL+name, queryParams)
	fmt.Println(u)
	body, resp, err := utils.SendHTTPRequest("PUT", u, client, jsonBack)
	newBackend := &Backend{}
	err = utils.HandleResponse(resp, body, http.StatusOK, newBackend)
	if err != nil {
		return nil, []byte(string(body)), fmt.Errorf("unexpected status code: %d", resp.StatusCode)

	}
	if err != nil {
		return nil, body, fmt.Errorf("unexpected status code: %d", resp.StatusCode)

	}
	return newBackend, body, nil
}
