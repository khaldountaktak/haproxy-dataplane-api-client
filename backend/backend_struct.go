package backend

var baseURL = "http://localhost:55/v2/services/haproxy/configuration/backends/"

type Backend struct {
	Version int  `json:"_version"`
	Data    Back `json:"data"`
}
type Backends struct {
	Version int    `json:"_version"`
	Data    []Back `json:"data"`
}
type Back struct {
	AdvCheck     string        `json:"adv_check"`
	Balance      BalanceConfig `json:"balance"`
	ForwardFor   ForwardFor    `json:"forwardfor"`
	HTTPChkParam HTTPChkParams `json:"httpchk_params"`
	Mode         string        `json:"mode"`
	Name         string        `json:"name"`
}

type BalanceConfig struct {
	Algorithm string `json:"algorithm"`
}

type ForwardFor struct {
	Enabled string `json:"enabled"`
}

type HTTPChkParams struct {
	Method  string `json:"method"`
	URI     string `json:"uri"`
	Version string `json:"version"`
}
